import {
    CREATE_IMAGE, CREATE_IMAGE_SUCCESS, DELETE_IMAGE,
    FACEBOOK_LOGIN, GET_IMAGES, GET_IMAGES_SUCCESS, IMAGE_FAILURE,
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGOUT_USER,
    REGISTER_USER,
    REGISTER_USER_SUCCESS,
    USER_FAILURE
} from "./actionTypes";

export const registerUserSuccess = user => {
    return {type: REGISTER_USER_SUCCESS, user};
};
export const userFailure = error => {
    return {type: USER_FAILURE, error};
};
export const registerUser = userData => {
    return {type: REGISTER_USER, userData};
};

export const loginUser = userData => {
    return {type: LOGIN_USER, userData};
};
export const loginUserSuccess = user => {
    return {type: LOGIN_USER_SUCCESS, user};
};

export const logoutUser = (user) => {
    return {type: LOGOUT_USER, user};
};
export const facebookLogin = data => {
    return {type: FACEBOOK_LOGIN, data};
};

export const createImage = image => {
    return {type: CREATE_IMAGE, image};
};
export const imageFailure = error => {
    return {type: IMAGE_FAILURE, error};
};
export const createImageSuccess = () => {
    return {type: CREATE_IMAGE_SUCCESS};
};
export const getImages = (images) => {
    return {type: GET_IMAGES, images};
};
export const getImagesSuccess = (images) => {
    return {type: GET_IMAGES_SUCCESS, images};
};
export const deleteImage = (image) => {
    return {type: DELETE_IMAGE, image};
};
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {loadFromLocalStorage, saveToLocalStorage} from "./localstorage";
import thunkMiddleware from "redux-thunk";
import createSagaMiddleware from "redux-saga";
import {rootSaga} from "./sagas";
import user from "./reducers/userReducer";
import image from "./reducers/imageReducer";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducers = combineReducers({
    user: user,
    image: image,
});

const persistedState = loadFromLocalStorage();

const sagaMiddleware = createSagaMiddleware();

const middleware = [
    thunkMiddleware,
    sagaMiddleware,
];

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const store = createStore(rootReducers, persistedState, enhancers);

sagaMiddleware.run(rootSaga);

store.subscribe(() => {
    saveToLocalStorage({
        user: {
            user: store.getState().user.user,
        }
    });
});

export default store;

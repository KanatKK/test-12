import {takeEvery} from "redux-saga/effects";
import {CREATE_IMAGE, DELETE_IMAGE, FACEBOOK_LOGIN, GET_IMAGES, LOGIN_USER, REGISTER_USER} from "../actionTypes";
import {facebookLoginSaga, loginUserSaga, registerUserSaga} from "./user";
import {createImageSaga, deleteImageSaga, getImagesSaga} from "./image";

export function* rootSaga() {
    yield takeEvery(REGISTER_USER, registerUserSaga);
    yield takeEvery(LOGIN_USER, loginUserSaga);
    yield takeEvery(FACEBOOK_LOGIN, facebookLoginSaga);
    yield takeEvery(CREATE_IMAGE, createImageSaga);
    yield takeEvery(GET_IMAGES, getImagesSaga);
    yield takeEvery(DELETE_IMAGE, deleteImageSaga);
}
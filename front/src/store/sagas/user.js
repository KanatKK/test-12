import {put} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {loginUserSuccess, registerUserSuccess, userFailure} from "../actions";

export function* registerUserSaga({userData}) {
    try {
        const response = yield axiosApi.post("/users", userData);
        yield put(registerUserSuccess(response.data));
    } catch(e) {
        if (e.response && e.response.data) {
            yield put(userFailure(e.response.data.error));
        } else {
            yield put(userFailure({global: "No internet"}));
        }
    }
}

export function* loginUserSaga({userData}) {
    try {
        const response = yield axiosApi.post("/users/sessions", userData);
        yield put(loginUserSuccess(response.data));
    } catch(e) {
        if (e.response && e.response.data) {
            yield put(userFailure(e.response.data.error));
        } else {
            yield put(userFailure({global: "No internet"}));
        }
    }
}

export function* facebookLoginSaga({data}) {
    try {
        const response = yield axiosApi.post("/users/facebookLogin", data);
        yield put(loginUserSuccess(response.data));
    } catch(e) {
        yield put(userFailure(e.response.data));
    }
}
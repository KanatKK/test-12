import {put} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {createImageSuccess, getImagesSuccess, imageFailure} from "../actions";

export function* createImageSaga({image}) {
    try {
        yield axiosApi.post("/images", image);
        yield put(createImageSuccess());
    } catch (e) {
        if (e.response && e.response.data) {
            yield put(imageFailure(e.response.data.error));
        } else {
            yield put(imageFailure({global: "No internet"}));
        }
    }
}

export function* getImagesSaga({images}) {
    if (images) {
        try {
            const response = yield axiosApi.get("/images?author=" + images);
            yield put(getImagesSuccess(response.data));
        } catch (e) {
            console.log(e);
        }
    } else {
        try {
            const response = yield axiosApi.get("/images");
            yield put(getImagesSuccess(response.data));
        } catch (e) {
            console.log(e);
        }
    }
}

export function* deleteImageSaga({image}) {
    try {
        yield axiosApi.delete("/images/" + image);
    } catch (e) {
        console.log(e)
    }
}
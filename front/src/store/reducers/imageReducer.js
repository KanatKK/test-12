import {CREATE_IMAGE_SUCCESS, GET_IMAGES_SUCCESS, IMAGE_FAILURE} from "../actionTypes";

const initialState = {
    images: null,
    error: "",
};

const image = (state = initialState, action) => {
    switch (action.type) {
        case GET_IMAGES_SUCCESS:
            return {...state, error: "", images: action.images};
        case CREATE_IMAGE_SUCCESS:
            return {...state, error: ""};
        case IMAGE_FAILURE:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default image;
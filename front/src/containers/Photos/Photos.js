import React, {useEffect} from 'react';
import Nav from "../../components/Nav/Nav";
import {useDispatch, useSelector} from "react-redux";
import {getImages} from "../../store/actions";
import UserImages from "../../components/ImagesList/UserImages";

const Photos = props => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user.user);

    useEffect(() => {
        dispatch(getImages(props.match.params.id));
    }, [dispatch, props.match.params.id]);

    useEffect(() => {
        const interval = setInterval(async () => {
            dispatch(getImages(props.match.params.id));
        }, 2000);
        return () => clearInterval(interval);
    }, [dispatch, props.match.params.id]);

    const toCreatePhoto = () => {
        props.history.push("/createPhoto");
    };

    return (
        <div className="container">
            <header>
                <h2>Photos</h2>
                <Nav
                    path="/" name="Gallery"
                />
            </header>
            <div className="content">
                {   user &&
                    user._id === props.match.params.id &&
                    <button
                        onClick={toCreatePhoto}
                        className="addPhotoBtn"
                    >
                        Add photo
                    </button>
                }
                <UserImages id={props.match.params.id}/>
            </div>
        </div>
    );
};

export default Photos;
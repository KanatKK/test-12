import React, {useEffect} from 'react';
import Nav from "../../components/Nav/Nav";
import {useDispatch, useSelector} from "react-redux";
import {getImages} from "../../store/actions";
import AllImages from "../../components/ImagesList/AllImages";

const MainPage = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user.user);

    useEffect(() => {
        dispatch(getImages());
    }, [dispatch]);

    return (
        <div className="container">
            <header>
                <h2>Gallery</h2>
                {user && <Nav path={"/myPhotos/" + user._id} name="My photos"/>}
                {!user && <Nav/>}
            </header>
            <div className="content">
                <AllImages/>
            </div>
        </div>
    );
};

export default MainPage;
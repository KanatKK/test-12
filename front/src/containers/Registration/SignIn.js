import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {registerUser} from "../../store/actions";
import FacebookLoginBtn from "../../components/FacebookLoginBtn/FacebookLoginBtn";

const SignIn = props => {
    const errors = useSelector(state => state.user.errors);
    const user = useSelector(state => state.user.user);
    const dispatch = useDispatch();

    if (user) {
        props.history.push("/");
    }

    const [state, setState] = useState({
        username: "",
        displayName: "",
        password: "",
        imageLink: "",
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };
    const formSubmitHandler = e => {
        e.preventDefault();
        dispatch(registerUser({...state}));
    };

    const toLogin = () => {
        props.history.push("/login");
    };

    return (
        <div className="container">
            <header>
                <h2>Create Account</h2>
            </header>
            <input
                type="text" placeholder="Add Username*"
                className="regFields" name="username"
                value={state.username} onChange={inputChangeHandler}
            />
            <input
                type="text" placeholder="Add Display Name*"
                className="regFields" name="displayName"
                value={state.displayName} onChange={inputChangeHandler}
            />
            <input
                type="text" placeholder="Add image url"
                className="regFields" name="imageLink"
                value={state.imageLink} onChange={inputChangeHandler}
            />
            <input
                type="password" placeholder="Create Password*"
                className="regFields" name="password"
                value={state.password} onChange={inputChangeHandler}
            />
            <button className="addBtn" onClick={formSubmitHandler}>Create</button>
            <FacebookLoginBtn/>
            {errors && <span className="error">{errors}</span>}
            <p className="changer" onClick={toLogin}>Already have an account? Login!</p>
        </div>
    );
};

export default SignIn;
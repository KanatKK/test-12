import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {loginUser} from "../../store/actions";
import FacebookLoginBtn from "../../components/FacebookLoginBtn/FacebookLoginBtn";

const Login = props => {
    const errors = useSelector(state => state.user.errors);
    const user = useSelector(state => state.user.user);
    const dispatch = useDispatch();

    const [state, setState] = useState({
        username: "",
        password: "",
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const formSubmitHandler = e => {
        e.preventDefault();
        dispatch(loginUser({...state}));
    };

    const toSignIn = () => {
        props.history.push("/signIn");
    };

    if (user) {
        props.history.push("/");
    }
    return (
        <div className="container">
            <header><h2>Login</h2></header>
            <input
                type="text" placeholder="Username*"
                className="regFields" name="username"
                value={state.username} onChange={inputChangeHandler}
            />
            <input
                type="password" placeholder="Password*"
                className="regFields" name="password"
                value={state.password} onChange={inputChangeHandler}
            />
            <button className="addBtn" onClick={formSubmitHandler}>Login</button>
            <FacebookLoginBtn/>
            {errors && <span className="error">{errors}</span>}
            <p className="changer" onClick={toSignIn}>Create Account</p>
        </div>
    );
};

export default Login;
import React from 'react';
import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import MainPage from "../MainPage/MainPage";
import SignIn from "../Registration/SignIn";
import Login from "../Registration/Login";
import Photos from "../Photos/Photos";
import CreatePhotos from "../CreatePhotos/CreatePhotos";

const App = () => {
  return (
      <BrowserRouter>
          <Switch>
              <Route path="/" exact component={MainPage}/>
              <Route path="/signIn" exact component={SignIn}/>
              <Route path="/login" exact component={Login}/>
              <Route path="/myPhotos/:id" exact component={Photos}/>
              <Route path="/createPhoto" exact component={CreatePhotos}/>
          </Switch>
      </BrowserRouter>
  );
};

export default App;

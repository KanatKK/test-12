import React, {useState} from 'react';
import Nav from "../../components/Nav/Nav";
import {useDispatch, useSelector} from "react-redux";
import {createImage} from "../../store/actions";

const CreatePhotos = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user.user);
    const error = useSelector(state => state.image.error);
    const [photo, setPhoto] = useState({title: "", image: null, author: user._id});

    const submitFormHandler = e => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(photo).forEach(key => {
            formData.append(key, photo[key]);
        });
        dispatch(createImage(formData));
        const photoCopy = {...photo};
        photoCopy.title = "";
        photoCopy.image = null;
        setPhoto(photoCopy);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setPhoto(prevState => {
            return {...prevState, [name]: value};
        });
    };
    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setPhoto(prevState => ({...prevState, [name]: file}));
    };

    return (
        <div className="container">
            <header>
                <h2>Create Photo</h2>
                <Nav
                    path="/" name="Gallery"
                />
            </header>
            <div className="createPhoto">
                <p
                    style={{marginBottom: 0}}
                >Title:</p>
                <input
                    type="text" className="createField" name="title"
                    style={{marginTop: 10}} placeholder="Add Title*"
                    onChange={inputChangeHandler} value={photo.title}
                />
                <p>Image:</p>
                <input
                    type="file" onChange={fileChangeHandler}
                    accept=".jpg, .jpeg, .png" name="image"
                />
                <button className="add" onClick={submitFormHandler}>Add</button>
                {error && <span className="error">{error}</span>}
            </div>
        </div>
    );
};

export default CreatePhotos;
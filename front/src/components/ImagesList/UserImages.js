import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deleteImage} from "../../store/actions";

const UserImages = props => {
    const dispatch = useDispatch();
    const images = useSelector(state => state.image.images);
    const user = useSelector(state => state.user.user);
    const [modal, setModal] = useState({display: "none", img: ""});

    const openModal = (img) => {
        setModal({display: "block", img: img});
    };
    const closeModal = () => {
        setModal({display: "none", img: ""});
    }

    const deleteImageHandler = (event) => {
        dispatch(deleteImage(event.target.id));
    } ;

    if (images) {
        const imagesList = images.map((img, ind) => {
            return (
                <div className="img" key={ind}>
                    <img id={img._id} onClick={() => openModal(img.image)} src={'http://localhost:8000/uploads/' + img.image} alt=""/>
                    <p>{img.title}</p>
                    {
                        user && user._id === props.id &&
                        <button
                            id={img._id} className="deleteBtn"
                            onClick={deleteImageHandler}
                        >Delete</button>
                    }
                </div>
            );
        });
        return (
            <>
                {
                    user && user._id !== props.id &&
                    <p>{images[0].author.displayName}'s gallery</p>
                }
                {!user && <p>{images[0].author.displayName}'s gallery</p>}
                <div className="imagesList">
                    <div className="modal" style={{display: modal.display}}>
                        {modal.img && <img src={'http://localhost:8000/uploads/' + modal.img} alt=""/>}
                        <button className="closeBtn" onClick={closeModal}>Close</button>
                    </div>
                    {imagesList}
                </div>
            </>
        );
    } else {
        return null;
    }
};

export default UserImages;
import React, {useState} from 'react';
import {useSelector} from "react-redux";
import {NavLink} from "react-router-dom";

const AllImages = props => {
    const images = useSelector(state => state.image.images);
    const [modal, setModal] = useState({display: "none", img: ""});

    const openModal = (img) => {
        setModal({display: "block", img: img});
    };
    const closeModal = () => {
        setModal({display: "none", img: ""});
    }

    if (images) {
        const imagesList = images.map((img, ind) => {
            return (
                <div className="img" key={ind}>
                    <img id={img._id} onClick={() => openModal(img.image)} src={'http://localhost:8000/uploads/' + img.image} alt=""/>
                    <p>{img.title}</p>
                    <p>By <NavLink
                        style={{color: "black"}}
                        className="author"
                        to={"/myPhotos/" + img.author._id}
                    >{img.author.displayName}</NavLink></p>
                </div>
            );
        });

        return (
            <div className="imagesList">
                <div className="modal" style={{display: modal.display}}>
                    {modal.img && <img src={'http://localhost:8000/uploads/' + modal.img} alt=""/>}
                    <button className="closeBtn" onClick={closeModal}>Close</button>
                </div>
                {imagesList}
            </div>
        );
    } else {
        return null
    }
};

export default AllImages;
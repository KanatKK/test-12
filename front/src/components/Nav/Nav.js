import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {NavLink} from "react-router-dom";
import {logoutUser} from "../../store/actions";

const Nav = props => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user.user);

    const logOut = () => {
        dispatch(logoutUser(user));
    };

    if (user) {
        return (
            <span className="nav">
                <NavLink
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer',
                        marginRight: 10,
                    }}
                    to={props.path}
                >
                    {props.name}
                </NavLink>
                <NavLink
                    onClick={logOut}
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer'
                    }}
                    to="/"
                >{user.displayName}</NavLink>
                <img className="userImage" src={user.imageLink} alt=""/>
            </span>
        );
    } else  {
        return (
            <span className="nav">
                <NavLink
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer',
                    }}
                    to="/signIn"
                >
                    Registration
                </NavLink>
            </span>
        );
    }

};

export default Nav;
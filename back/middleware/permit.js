const permit = (...roles) => {
    return (req, res, next) => {
        if (!req.author) {
            return res.status(401).send({message: "Unauthenticated"});
        }
        if (!roles.includes(req.author.role)) {
            return res.status(403).send({message: "Unauthorized"});
        }
        next();
    };
};

module.exports = permit;
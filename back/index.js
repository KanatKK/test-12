const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const config = require("./config");
const user = require("./app/users");
const image = require("./app/images");
const app = express();
const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

const run = async () => {
    await mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true});

    app.use("/users", user);
    app.use("/images", image);

    app.listen(port, () => {
        console.log(`Server started at http://localhost:${port}`);
    });
};

run().catch(console.log);
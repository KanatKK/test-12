const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");
const auth = require("../middleware/auth");
const Image = require("../models/Image")

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get("/", async (req, res) => {
    if (req.query.author) {
        try {
            const images = await Image.find({author: req.query.author}).populate("author");
            res.send(images);
        } catch (e) {
            res.status(500).send(e);
        }
    } else {
        try {
            const images = await Image.find().populate("author");
            res.send(images);
        } catch (e) {
            res.status(500).send(e);
        }
    }
});

router.post("/", upload.single("image"), auth, async (req, res) => {
    const imageData = req.body;
    if (req.file) {
        imageData.image = req.file.filename;
    }
    if (!req.file) {
        return res.status(400).send({error: "Please, add image."});
    }
    if (imageData.title === "") {
        return res.status(400).send({error: "Please, add title."});
    }
    const image = new Image(imageData);
    try {
        await image.save();
        res.send(image);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete("/:id", auth, async (req, res) => {
    try {
        await Image.findByIdAndDelete(req.params.id);
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;
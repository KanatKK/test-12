const mongoose = require("mongoose");
const config = require("./config");
const User = require("./models/User");
const Image = require("./models/Image");
const {nanoid} = require("nanoid");

mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true});
const db = mongoose.connection;

db.once("open", async () => {
    try {
        await db.dropCollection("users");
        await db.dropCollection("images");
    } catch (e) {
        console.log("Collection were not presented, skipping drop...");
    }
    const [Rick, Stewie] = await User.create({
        username: "user",
        displayName: "Rick",
        imageLink: "https://avatarfiles.alphacoders.com/128/128984.png",
        role: "user",
        token: nanoid(),
        password: "1234"
    }, {
        username: "user2",
        displayName: "Stewie",
        imageLink: "https://pbs.twimg.com/profile_images/2583106733/6tfnwxuniihd09lajloc_400x400.png",
        role: "user",
        token: nanoid(),
        password: "4321"
    });
    await Image.create({
        title : "bmw",
        author : Rick._id,
        image : "Ubg9V1vnY9UaqRmkGy0t8.jpg",
    }, {
        title : "rolls royce",
        author : Rick._id,
        image : "w0mQ0qTI3i1uswjNvb2Qn.jpg",
    },{
        title : "Cocktail",
        "author" : Rick._id,
        image : "3kqytSVvSbBnm82_L6zFZ.jpg",
    },{
        title : "Mojito",
        author : Stewie._id,
        image : "4zc_dDZ5dHyx9z6Lt6by_.jpg",
    },{
        title : "Some image",
        author : Stewie._id,
        image : "bA-6EZNHQSuRzcLn3WVDx.png",
    },{
        title : "Test",
        author : Stewie._id,
        image : "qfTSHu2alpTNKioDJPRaV.jpg",
    });

    db.close();
});
